<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChampionRepository")
 */
class Champion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $champ_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tips", mappedBy="champ_name")
     */
    private $tips;

    public function __construct()
    {
        $this->matchUps = new ArrayCollection();
        $this->tips = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChampName(): ?string
    {
        return $this->champ_name;
    }

    public function setChampName(string $Champ_name): self
    {
        $this->champ_name = $Champ_name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|MatchUp[]
     */
    public function getMatchUps(): Collection
    {
        return $this->matchUps;
    }

    public function addMatchUp(MatchUp $matchUp): self
    {
        if (!$this->matchUps->contains($matchUp)) {
            $this->matchUps[] = $matchUp;
            $matchUp->addChampName($this);
        }

        return $this;
    }

    public function removeMatchUp(MatchUp $matchUp): self
    {
        if ($this->matchUps->contains($matchUp)) {
            $this->matchUps->removeElement($matchUp);
            $matchUp->removeChampName($this);
        }

        return $this;
    }

    /**
     * @return Collection|Tips[]
     */
    public function getTips(): Collection
    {
        return $this->tips;
    }

    public function addTip(Tips $tip): self
    {
        if (!$this->tips->contains($tip)) {
            $this->tips[] = $tip;
            $tip->addChampName($this);
        }

        return $this;
    }

    public function removeTip(Tips $tip): self
    {
        if ($this->tips->contains($tip)) {
            $this->tips->removeElement($tip);
            $tip->removeChampName($this);
        }

        return $this;
    }
}
