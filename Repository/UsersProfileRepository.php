<?php

namespace App\Repository;

use App\Entity\UsersProfile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsersProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersProfile|null findOneByPseudo(string $pseudo)
 * @method UsersProfile[]    findAll()
 * @method UsersProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersProfileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersProfile::class);
    }

    // /**
    //  * @return UsersProfile[] Returns an array of UsersProfile objects
    //  */

    public function findByPseudo($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.pseudo = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?UsersProfile
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
