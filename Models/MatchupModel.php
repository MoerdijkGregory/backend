<?php


namespace App\Models;


class MatchupModel
{
    public $champ_adv;
    public $victory;
    public $lane;
    public $champ_name;
    public $pseudo;
    public $tips;
}