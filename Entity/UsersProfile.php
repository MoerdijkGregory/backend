<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersProfileRepository")
 * @UniqueEntity("email", message="email déja prit")
 */
class UsersProfile implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rank;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MatchUp", mappedBy="pseudo")
     */
    private $matchups;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tips", inversedBy="usersProfiles")
     */
    private $tips;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", mappedBy="userprofile_id")
     */
    private $roles;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Groupe", inversedBy="usersProfiles")
     */
    private $groupe;

//    /**
//     * @var UploadedFile
//     */
//    private $file;
//
//    /**
//     * @return UploadedFile
//     */
//    public function getFile()
//    {
//        return $this->file;
//    }
//
//    /**
//     * @param mixed $file
//     */

    public function setFile($file): void
    {
        $this->file = $file;
    }

    public function __construct()
    {
        $this->matchups = new ArrayCollection();
        $this->tips = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRank(): ?string
    {
        return $this->rank;
    }

    public function setRank(string $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection|Matchup[]
     */
    public function getMatchups(): Collection
    {
        return $this->matchups;
    }

    public function addMatchup(Matchup $matchup): self
    {
        if (!$this->matchups->contains($matchup)) {
            $this->matchups[] = $matchup;
            $matchup->setPseudo($this);
        }

        return $this;
    }

    public function removeMatchup(Matchup $matchup): self
    {
        if ($this->matchups->contains($matchup)) {
            $this->matchups->removeElement($matchup);
            // set the owning side to null (unless already changed)
            if ($matchup->getPseudo() === $this) {
                $matchup->setPseudo(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tips[]
     */
    public function getTips(): Collection
    {
        return $this->tips;
    }

    public function addTip(Tips $tip): self
    {
        if (!$this->tips->contains($tip)) {
            $this->tips[] = $tip;
        }

        return $this;
    }

    public function removeTip(Tips $tip): self
    {
        if ($this->tips->contains($tip)) {
            $this->tips->removeElement($tip);
        }

        return $this;
    }


    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        if(!$this->getGroupe()) return [];
        return  [$this->getGroupe()->getRole()[0]->getLabel()];
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
            $role->addUserprofileId($this);
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
            $role->removeUserprofileId($this);
        }

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }
    // implementation userinterface
    public function getUsername()
    {
       return $this->email;
    }
    public function getSalt()
    {
        return null;
    }
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
