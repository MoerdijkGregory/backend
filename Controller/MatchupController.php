<?php

namespace App\Controller;

use App\DTO\ChampionDto;
use App\DTO\MatchUpDto;
use App\DTO\TipsOnMatchUpDTO;
use App\DTO\UserDto;
use App\Entity\Champion;
use App\Entity\MatchUp;
use App\Entity\Tips;
use App\Entity\UsersProfile;
use App\Form\MatchUpType;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

class MatchupController extends AbstractController
{
    /**
     * @Get(
     *     path = "api/matchup/{id}",
     *     name = "app_matchup_show",
     *     requirements = {"id"="\d+"}
     * )
     * @View()
     */
    public function showAction(MatchUp $matchup)
    {
        return $matchup;
    }

    /**
     * @Post(
     *    path = "api/matchup",
     *    name = "app_matchup_create"
     * )
     * @View()
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
//        $matchup = new MatchUp();
//        $form = $this->createForm(MatchUpType::class, $matchup);
//        $form->submit(json_decode($request->getContent(),true));
//        $em = $this->getDoctrine()->getManager();
//
//        $em->persist($matchup);
//        $em->flush();
//
//        return $matchup;

        $form = $this->createForm(MatchUpType::class, null);
        $form->submit(json_decode($request->getContent(),true));
        $em = $this->getDoctrine()->getManager();
        $data = $form->getData();
        dump($data);
        /** @var UsersProfile $userProfile */
        $userProfile = $this->getDoctrine()->getRepository(UsersProfile::class)->findOneByPseudo($data->pseudo);
        if(!$userProfile) throw new \Exception("Pseudo non trouvé");

        /** @var Champion $champname */
        $champname = $this->getDoctrine()->getRepository(Champion::class)->findOneByName($data->champ_name);
        if(!$champname) throw new \Exception("Champion non trouvé");

        /** @var Champion $champadv */
        $champadv = $this->getDoctrine()->getRepository(Champion::class)->findOneByName($data->champ_adv);
        if(!$champadv) throw new \Exception("Champion adverse non trouvé");

        $tips = array_map(function($tip) {
            return $this->getDoctrine()->getRepository(Tips::class)->findOneByName($tip);
        }, explode(',' ,$data->tips));


        $matchup = new MatchUp();
        $matchup->setVictory($data->victory);
        $matchup->setLane($data->lane);
        $matchup-> setChampName($champname);
        $matchup-> setChampAdv($champadv);
        $matchup-> setPseudo($userProfile);

        foreach($tips as $tip) {
            $matchup -> addTip($tip);
        }



        $championnameDto = new ChampionDto();
        $championnameDto->id = $champname->getId();
        $championnameDto->name = $champname->getChampName();
        $championnameDto->image = $champname->getImage();
        $championnameDto->tips = array_map(function (Tips $tip) {
            $tipsDto = new TipsOnMatchUpDTO();
            $tipsDto->id = $tip->getId();
            $tipsDto->name = $tip->getName();
            return $tipsDto;
        }, $champname->getTips()->toArray());

        $championadvDto = new ChampionDto();
        $championadvDto->id = $champadv->getId();
        $championadvDto->name = $champadv->getChampName();
        $championadvDto->image = $champadv->getImage();
        $championadvDto->tips = array_map(function (Tips $tip) {
            $tipsDto = new TipsOnMatchUpDTO();
            $tipsDto->id = $tip->getId();
            $tipsDto->name = $tip->getName();
            return $tipsDto;
        }, $champadv->getTips()->toArray());

        $userProfileDto = new UserDto();
        $userProfileDto->id = $userProfile->getId();
        $userProfileDto->pseudo = $userProfile->getPseudo();
        $userProfileDto->email = $userProfile->getEmail();
        $userProfileDto->rank = $userProfile->getRank();
        $userProfileDto->avatar = $userProfile->getAvatar();
        $userProfileDto->tips = array_map(function (Tips $tip) {
            $tipsDto = new TipsOnMatchUpDTO();
            $tipsDto->id = $tip->getId();
            $tipsDto->name = $tip->getName();
            return $tipsDto;
        }, $userProfile->getTips()->toArray());

        $matchupDto = new MatchUpDto();
        $matchupDto->id = $champname->getId();
        $matchupDto->champ_name = $championnameDto;
        $matchupDto->champ_adv = $championadvDto;
        $matchupDto->lane = $data->lane;
        $matchupDto->victory = $data->victory ;
        $matchupDto->pseudo = $userProfileDto;
        $matchupDto->tips = array_map(function (Tips $tip) {
            $tipsDto = new TipsOnMatchUpDTO();
            $tipsDto->id = $tip->getId();
            $tipsDto->name = $tip->getName();
            return $tipsDto;
        }, $matchup->getTips()->toArray());


        $em->persist($matchup);
        $em->flush();

        return $matchupDto;
    }

    /**
     * @Get(
     *     path="api/matchup_list",
     *     name="app_matchup_list"
     * )
     * @View()php
     */
    public  function listAction( )
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(MatchUp::class);
        return $repository->findAll();
    }

    /**
     * @Get(
     *     path="api/user_matchup/{pseudo}",
     *     name="app_user_matchup"
     * )
     * @View()php
     */
    public  function userMatchup(string $pseudo )
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(MatchUp::class);
        $array = [];

        $data = $repository->findByUser($pseudo);

        foreach ($data as $matchupEntity)
        {
            /**
             * @var MatchUp $matchupEntity
             */
            $dto = new MatchupDto();
            $dto->id = $matchupEntity->getId();
            $dto->champ_name = $matchupEntity->getChampName()->getChampName();
            $dto->champ_adv = $matchupEntity->getChampAdv()->getChampName();
            $dto->victory = $matchupEntity->getVictory();
            $dto->lane = $matchupEntity->getLane();
            $dto->pseudo = $matchupEntity->getPseudo()->getPseudo();
            $dto->tips = array_map(function(Tips $tip) {
                $tipsDto = new TipsOnMatchUpDTO();
                $tipsDto->id = $tip->getId();
                $tipsDto->name = $tip->getName();
                return $tipsDto;
            },  $matchupEntity->getTips()->toArray());

            $array[] = $dto;
        }

        return $array;
    }
    /**
     * @Get(
     *     path="api/user_matchup_last/{pseudo}",
     *     name="app_user_matchup_last"
     * )
     * @View()php
     */
    public  function userLastMatchup(string $pseudo )
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(MatchUp::class);
        $array = [];
        $data = $repository->findByUserProf($pseudo);

        foreach ($data as $matchupEntity) {
            /**
             * @var MatchUp $matchupEntity
             */
            $dto = new MatchupDto();
            $dto->id = $matchupEntity->getId();
            $dto->champ_name = $matchupEntity->getChampName()->getChampName();
            $dto->champ_adv = $matchupEntity->getChampAdv()->getChampName();
            $dto->victory = $matchupEntity->getVictory();
            $dto->lane = $matchupEntity->getLane();
            $dto->pseudo = $matchupEntity->getPseudo()->getPseudo();
            $dto->tips = array_map(function (Tips $tip) {
                $tipsDto = new TipsOnMatchUpDTO();
                $tipsDto->id = $tip->getId();
                $tipsDto->name = $tip->getName();
                return $tipsDto;
            }, $matchupEntity->getTips()->toArray());

            $array[] = $dto;
        }


        return $array;
    }

    /**
     * @Get(
     *     path="api/user_laneAvg/{pseudo}",
     *     name="app_user_laneAvg"
     * )
     * @View()php
     */
    public  function userLaneAvg(string $pseudo )
    {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT m.lane, SUM(m.victory) as SumVictory, COUNT(m.victory) as TotVictory , SUM(m.victory)/COUNT(m.victory)*100 as  pourcentage FROM App\Entity\MatchUp m JOIN m.pseudo as p WHERE p.pseudo = :pseudo GROUP BY m.lane');
            $query->setParameter('pseudo', $pseudo);
//            dump($query->getResult());

        return $query->getResult();
    }

    /**
     * @Get(
     *     path="api/allLaneAvg",
     *     name="app_allLaneAvg"
     * )
     * @View()php
     */
    public  function allLaneAvg()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT m.lane, SUM(m.victory) as SumVictory, COUNT(m.victory) as TotMatch , SUM(m.victory)/COUNT(m.victory)*100 as  pourcentage FROM App\Entity\MatchUp m  GROUP BY m.lane');
//            dump($query->getResult());

        return $query->getResult();
    }
    /**
     * @Get(
     *     path="api/counter/{champadv}",
     *     name="app_counter"
     * )
     * @View()php
     */
    public  function Counter(string $champadv)
    {

        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(MatchUp::class);
        $data = $repository->findCounter($champadv);

        return $data;

    }
}
