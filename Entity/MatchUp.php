<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatchUpRepository")
 */
class MatchUp
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Champion")
     */
    private $champ_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Champion")
     */
    private $champ_adv;

    /**
     * @ORM\Column(type="boolean")
     */
    private $victory;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lane;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsersProfile", inversedBy="matchups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pseudo;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tips", inversedBy="matchUps")
     */
    private $tips;

    public function __construct()
    {
        $this->tips = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Champion
     */
    public function getChampName(): ?Champion
    {
        return $this->champ_name;
    }
    /**
     * @return Champion
     */
    public function setChampName(Champion $champion)
    {
        $this->champ_name = $champion;
    }

    public function getChampAdv(): ?Champion
    {
        return $this->champ_adv;
    }

    public function setChampAdv(Champion $champ_adv): self
    {
        $this->champ_adv = $champ_adv;

        return $this;
    }

    public function getVictory(): ?bool
    {
        return $this->victory;
    }

    public function setVictory(bool $victory): self
    {
        $this->victory = $victory;

        return $this;
    }

    public function getLane(): ?string
    {
        return $this->lane;
    }

    public function setLane(string $lane): self
    {
        $this->lane = $lane;

        return $this;
    }

    public function getPseudo(): ?UsersProfile
    {
        return $this->pseudo;
    }

    public function setPseudo(?UsersProfile $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @return Collection|Tips[]
     */
    public function getTips(): Collection
    {
        return $this->tips;
    }

    public function addTip(Tips $tip): self
    {
        if (!$this->tips->contains($tip)) {
            $this->tips[] = $tip;
        }

        return $this;
    }

    public function removeTip(Tips $tip): self
    {
        if ($this->tips->contains($tip)) {
            $this->tips->removeElement($tip);
        }

        return $this;
    }
}
