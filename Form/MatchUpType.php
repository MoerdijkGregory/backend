<?php

namespace App\Form;

use App\Models\MatchupModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatchUpType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('champ_adv')
            ->add('victory')
            ->add('lane')
            ->add('champ_name')
            ->add('pseudo')
            ->add('tips')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MatchupModel::class,
        ]);
    }
}
