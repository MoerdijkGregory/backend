<?php

namespace App\Repository;

use App\Entity\Tips;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tips|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tips|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tips[]    findAll()
 * @method Tips[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TipsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tips::class);
    }

     /**
      * @return Tips[] Returns an array of Tips objects
      */

    public function findByChampName($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.champ_name = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Tips[] Returns an array of Tips objects
     */

    public function findByPseudo($pseudo)
    {
        return $this->createQueryBuilder('t')->join("t.usersProfiles" , "u")
            ->andWhere('u.pseudo = :val')
            ->setParameter('val', $pseudo)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    /**
     * @return Tips[] Returns an array of Tips objects
     */

    public function findGeneralType($type)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.tip_type = :val')
            ->setParameter('val', $type)
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }


}
