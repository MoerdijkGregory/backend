<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TipsRepository")
 */
class Tips
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tip_type;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Champion", inversedBy="tips")
     */
    private $champ_name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\MatchUp", mappedBy="tips")
     */
    private $matchUps;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UsersProfile", mappedBy="tips")
     */
    private $usersProfiles;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;



    public function __construct()
    {
        $this->champ_name = new ArrayCollection();
        $this->matchUps = new ArrayCollection();
        $this->usersProfiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipType(): ?string
    {
        return $this->tip_type;
    }

    public function setTipType(string $tip_type): self
    {
        $this->tip_type = $tip_type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return Collection|Champion[]
     */
    public function getChampName(): Collection
    {
        return $this->champ_name;
    }

    public function addChampName(Champion $champName): self
    {
        if (!$this->champ_name->contains($champName)) {
            $this->champ_name[] = $champName;
        }

        return $this;
    }

    public function removeChampName(Champion $champName): self
    {
        if ($this->champ_name->contains($champName)) {
            $this->champ_name->removeElement($champName);
        }

        return $this;
    }

    /**
     * @return Collection|MatchUp[]
     */
    public function getMatchUps(): Collection
    {
        return $this->matchUps;
    }

    public function addMatchUp(MatchUp $matchUp): self
    {
        if (!$this->matchUps->contains($matchUp)) {
            $this->matchUps[] = $matchUp;
            $matchUp->addTip($this);
        }

        return $this;
    }

    public function removeMatchUp(MatchUp $matchUp): self
    {
        if ($this->matchUps->contains($matchUp)) {
            $this->matchUps->removeElement($matchUp);
            $matchUp->removeTip($this);
        }

        return $this;
    }

    /**
     * @return Collection|UsersProfile[]
     */
    public function getUsersProfiles(): Collection
    {
        return $this->usersProfiles;
    }

    public function addUsersProfile(UsersProfile $usersProfile): self
    {
        if (!$this->usersProfiles->contains($usersProfile)) {
            $this->usersProfiles[] = $usersProfile;
            $usersProfile->addTip($this);
        }

        return $this;
    }

    public function removeUsersProfile(UsersProfile $usersProfile): self
    {
        if ($this->usersProfiles->contains($usersProfile)) {
            $this->usersProfiles->removeElement($usersProfile);
            $usersProfile->removeTip($this);
        }

        return $this;
    }

}
