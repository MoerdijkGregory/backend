<?php


namespace App\DTO;


class MatchUpDto
{
    public $id;
    public $champ_name;
    public $champ_adv;
    public $victory;
    public $lane;
    public $pseudo;
    public $tips;
}