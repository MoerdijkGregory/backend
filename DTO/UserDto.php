<?php


namespace App\DTO;


class UserDto
{
    public $id;
    public $pseudo;
    public $email;
    public $rank ;
    public $avatar;
    public $password;
    public $groupename;
}