<?php

namespace App\Controller;

use App\DTO\ChampionDto;
use App\DTO\TipsOnMatchUpDTO;
use App\Entity\Champion;
use App\Entity\Tips;
use App\Form\ChampionType;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;

class ChampionController extends AbstractController
{
    /**
     * @Get(
     *     path = "api/champion/{champion}",
     *     name = "app_champ_show"
     * )
     * @View()
     */
    public function showAction(string $champion)
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(Champion::class);
        /** @var Champion $championEntity */
        $championEntity = $repository->findOneByName($champion);

        $dto = new ChampionDto();
        $dto->id = $championEntity->getId();
        $dto->name = $championEntity->getChampName();
        $dto->image = $championEntity->getImage();
        $dto->tips = array_map(function (Tips $tip) {
            $tipsDto = new TipsOnMatchUpDTO();
            $tipsDto->id = $tip->getId();
            $tipsDto->name = $tip->getName();
            return $tipsDto;
        }, $championEntity->getTips()->toArray());

        return $dto;
    }
    /**
     * @Post(
     *    path = "api/champion",
     *    name = "app_champ_create"
     * )
     * @View()
*/
    public function createAction(Request $request)
    {
        $champion = new Champion();
        $form = $this->createForm(ChampionType::class, $champion);
        $form->submit(json_decode($request->getContent(),true));
        $em = $this->getDoctrine()->getManager();

        $em->persist($champion);
        $em->flush();

        return $champion;
    }

    /**
     * @Get(
     *     path="api/champion",
     *     name="app_champ_list"
     * )
     * @View()php
     */
    public  function listAction( )
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(Champion::class);
        $array = [];

        $data = $repository->findAll();

        foreach ($data as $championEntity) {
            $dto = new ChampionDto();
            $dto->id = $championEntity->getId();
            $dto->name = $championEntity->getChampName();
            $dto->image = $championEntity->getImage();
            $array[] = $dto;
        }

        return $array;
    }
}
