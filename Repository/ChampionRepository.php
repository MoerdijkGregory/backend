<?php

namespace App\Repository;

use App\Entity\Champion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Champion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Champion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Champion[]    findAll()
 * @method Champion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChampionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Champion::class);
    }

     /**
      * @return Champion[] Returns an array of Champion objects
      */

    public function findOneByName($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.champ_name = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getSingleResult()
        ;
    }


//    public function findByChampname($value): ?Champion
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.champ_name = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
