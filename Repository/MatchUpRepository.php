<?php

namespace App\Repository;

use App\Entity\MatchUp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MatchUp|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchUp|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchUp[]    findAll()
 * @method MatchUp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchUpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchUp::class);
    }

        /**
     * @return MatchUp[] Returns an array of MatchUp objects
     */

    public function findByUser(string $pseudo)
    {
        return $this->createQueryBuilder('m')->join("m.pseudo" , "p")
            ->andWhere('p.pseudo = :val')
            ->setParameter('val', $pseudo)
            ->orderBy('m.id', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }
    public function findByUserProf(string $pseudo)
    {
        return $this->createQueryBuilder('m')->join("m.pseudo" , "p")
            ->andWhere('p.pseudo = :val')
            ->setParameter('val', $pseudo)
            ->orderBy('m.id', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
            ;
    }


    public function findCounter(string $champadv)
    {
        return $this->createQueryBuilder('m')
            ->select('c.champ_name, SUM(m.victory) / COUNT(m.id) * 100 as pourcentage')
            ->join("m.champ_name" , "c")
            ->join("m.champ_adv" , "ca")
            ->andWhere('ca.champ_name = :val')
            ->setParameter('val', $champadv)
            ->groupBy('m.champ_name')
            ->orderBy('pourcentage', 'DESC' )
            ->getQuery()
            ->getResult()
        ;
    }
}
