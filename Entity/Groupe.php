<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupeRepository")
 */
class Groupe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", inversedBy="groupes")
*/
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UsersProfile", mappedBy="groupe")
     */
    private $usersProfiles;

    public function __construct()
    {
        $this->role = new ArrayCollection();
        $this->usersProfiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getRole(): Collection
    {
        return $this->role;
    }

    public function addRole(Role $role): self
    {
        if (!$this->role->contains($role)) {
            $this->role[] = $role;
        }

        return $this;
    }

    public function removeRole(Role $role): self
    {
        if ($this->role->contains($role)) {
            $this->role->removeElement($role);
        }

        return $this;
    }

    /**
     * @return Collection|UsersProfile[]
     */
    public function getUsersProfiles(): Collection
    {
        return $this->usersProfiles;
    }

    public function addUsersProfile(UsersProfile $usersProfile): self
    {
        if (!$this->usersProfiles->contains($usersProfile)) {
            $this->usersProfiles[] = $usersProfile;
            $usersProfile->setGroupe($this);
        }

        return $this;
    }

    public function removeUsersProfile(UsersProfile $usersProfile): self
    {
        if ($this->usersProfiles->contains($usersProfile)) {
            $this->usersProfiles->removeElement($usersProfile);
            // set the owning side to null (unless already changed)
            if ($usersProfile->getGroupe() === $this) {
                $usersProfile->setGroupe(null);
            }
        }

        return $this;
    }
}
