<?php

namespace App\Controller;

use App\DTO\UserDto;
use App\Entity\Groupe;
use App\Entity\UsersProfile;
use App\Form\UserType;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Get(
     *     path = "api/users/{pseudo}",
     *     name = "app_user_profile_show"
     * )
     * @View()
     */
    public function showAction(string  $pseudo)
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(UsersProfile::class);
        return $repository->findByPseudo($pseudo);
    }
    /**
     * @Post(
     *    path = "api/user",
     *    name = "app_user_create"
     * )
     * @View()
     */
    public function createAction(Request $request,UserPasswordEncoderInterface $encoder)
    {
        $user = new UsersProfile();
        $form = $this->createForm(UserType::class, $user);
        $form->submit(json_decode($request->getContent(),true));
        $em = $this->getDoctrine()->getManager();

        $hash = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($hash); // encodage du password
        $group = $this->getDoctrine()->getRepository(Groupe::class)->findOneByLabel("User_Group");
        $user->setGroupe($group);
        $dto = new UserDto();
        $dto->pseudo = $user->getPseudo();
        $dto->email = $user->getEmail();
        $dto->rank = $user->getRank();
        $dto->password = $user->getPassword();
        $dto->groupename = $user->getGroupe()->getLabel();

//        $user->setAvatar(uniqid());
//        $user->getFile()->move("../public");
        dump($user);

        $em->persist($user);
        $em->flush();

        return $dto;
    }
    /**
     * @Get(
     *     path="api/users",
     *     name="app_users_list"
     * )
     * @View()
     */
    public  function listAction( )
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(UsersProfile::class);
        return $repository->findAll();
    }
}
