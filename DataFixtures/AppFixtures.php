<?php

namespace App\DataFixtures;

use App\Entity\Champion;
use App\Entity\Groupe;
use App\Entity\MatchUp;
use App\Entity\Role;
use App\Entity\Tips;
use App\Entity\UsersProfile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // user fixture
        $listrank = ["iron", "bronze", "gold" , "platine" , "diamond"];
        $listlane = ["Top" , "Mid" , "Jungle" , "Adc" , "Support"];
        $listtype = ["General" , "Champion","User" , "Matchup"];
        $listchamp = $manager->getRepository(Champion::class)->findAll();
        $listuser = $manager->getRepository(UsersProfile::class)->findAll();
        $listmatchup = $manager->getRepository(MatchUp::class)->findAll();

//         groupe role fixture
        $role1 = new Role();
        $role1->setLabel("ROLE_ADMIN");
        $manager->persist($role1);
        $role2 = new Role();
        $role2->setLabel("ROLE_USERS");
        $manager->persist($role2);

        $groupe1 = new Groupe();
        $groupe1->addRole($role1);
        $groupe1->setLabel("Admin_Group");
        $manager->persist($groupe1);
        $groupe2 = new Groupe();
        $groupe2->addRole($role2);
        $groupe2->setLabel("User_Group");
        $manager->persist($groupe2);

        for ($count = 0; $count < 20; $count++) {
            $r = random_int(0, count( $listrank)-1);

            $user = new UsersProfile();
            $user->setRank($listrank[$r]);
            $user->setPseudo("Pseudo " . $count);
            $user->setEmail("random" . $count . "@hotmail.com");
            $user->setPassword("mdp" . $count);
            $user->setAvatar("avatar" . $count);
            $user->setGroupe($groupe2);
            $manager->persist($user);
            for ($count2 = 0; $count2 < 10; $count2++)
            {
                $matchup = new MatchUp();
                $matchup->setPseudo( $user);
                $r1 = random_int(0, count($listchamp)-1);
                $matchup->setChampName($listchamp[$r1]);
                $r1 = random_int(0, count($listchamp)-1);
                $matchup->setChampAdv($listchamp[$r1]);
                $r = random_int(0, count( $listlane)-1);
                $matchup->setLane($listlane[$r]);
                $matchup->setVictory(random_int(0,1));
                $manager->persist($matchup);
            }
        }
//         tips fixtures
        for ($i = 0; $i < count($listchamp); $i++)
        {
            for ($count3 = 0; $count3 < 10; $count3++)
            {
                $tips = new Tips();
                $tips->setName("tips champion" . $count3);
                $tips->setTipType("Champion");
                $tips->addChampName($listchamp[$i]);
                $manager->persist($tips);
            }
        }
        for ($u = 0; $u < count($listuser); $u++)
        {
            for ($count3 = 0; $count3 < 10; $count3++)
            {
                $tips2 = new Tips();
                $tips2->setName("tips User" . $count3);
                $tips2->setTipType("User");
                $tips2->addUsersProfile($listuser[$u]);
                $manager->persist($tips2);
            }
        }

        for ($count3 = 0; $count3 < 20; $count3++)
        {
            $tips3 = new Tips();
            $tips3->setName("tips General" . $count3);
            $tips3->setTipType("General");
            $manager->persist($tips3);
        }

        for ($m = 0; $m < count($listmatchup); $m++)
        {
            for ($count3 = 0; $count3 < 5; $count3++)
            {
                $tips4 = new Tips();
                $tips4->setName("tips User" . $count3);
                $tips4->setTipType("User");
                $tips4->addMatchUp($listmatchup[$m]);
                $manager->persist($tips4);
            }
        }






        $manager->flush();
    }
}
