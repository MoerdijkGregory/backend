<?php

namespace App\Controller;


use App\DTO\TipsOnMatchUpDTO;
use App\Entity\Tips;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TipsController extends AbstractController
{
    /**
     * @Get(
     *     path = "api/tips/{champName}",
     *     name = "app_tips_champ_show"
     * )
     * @View()
     */
    public function showChampTips(string  $champName)
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(Tips::class);
        return $repository->findByChampName($champName);
    }
    /**
     * @Get(
     *     path = "api/usertips/{pseudo}",
     *     name = "app_usertips_champ_show"
     * )
     * @View()
     */
    public function showUserTips(string  $pseudo)
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(Tips::class);
        $array = [];

        $data = $repository->findByPseudo($pseudo);

        foreach ($data as $championEntity) {
            $dto = new TipsOnMatchUpDTO();
            $dto->id = $championEntity->getId();
            $dto->name = $championEntity->getName();
            $array[] = $dto;
        }

        return $array;
    }
    /**
     * @Get(
     *     path = "api/generaltips",
     *     name = "app_generaltips_show"
     * )
     * @View()
     */
    public function showGeneralTips()
    {
        $doctrine = $this->getDoctrine();
        $repository = $doctrine->getRepository(Tips::class);
        $array = [];
        $type = 'General';

        $data = $repository->findGeneralType($type);

        foreach ($data as $tipsEntity) {
            $dto = new TipsOnMatchUpDTO();
            $dto->id = $tipsEntity->getId();
            $dto->name = $tipsEntity->getName();
            $array[] = $dto;
        }

        return $array;
    }

}
