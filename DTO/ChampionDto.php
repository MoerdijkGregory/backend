<?php


namespace App\DTO;


class ChampionDto
{
    public $id;
    public $name;
    public $image;
    public $tips;
}