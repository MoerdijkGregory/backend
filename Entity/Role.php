<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\UsersProfile", inversedBy="roles")
     */
    private $userprofile_id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Groupe", mappedBy="role")
     */
    private $groupes;

    public function __construct()
    {
        $this->userprofile_id = new ArrayCollection();
        $this->groupes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|UsersProfile[]
     */
    public function getUserprofileId(): Collection
    {
        return $this->userprofile_id;
    }

    public function addUserprofileId(UsersProfile $userprofileId): self
    {
        if (!$this->userprofile_id->contains($userprofileId)) {
            $this->userprofile_id[] = $userprofileId;
        }

        return $this;
    }

    public function removeUserprofileId(UsersProfile $userprofileId): self
    {
        if ($this->userprofile_id->contains($userprofileId)) {
            $this->userprofile_id->removeElement($userprofileId);
        }

        return $this;
    }

    /**
     * @return Collection|Groupe[]
     */
    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): self
    {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes[] = $groupe;
            $groupe->addRole($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): self
    {
        if ($this->groupes->contains($groupe)) {
            $this->groupes->removeElement($groupe);
            $groupe->removeRole($this);
        }

        return $this;
    }
}
